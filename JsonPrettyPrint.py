import sublime
import sublime_plugin
import json


class JsonPrettyPrintCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        # Get the entire region
        view_region = sublime.Region(0, self.view.size())
        # Get the region text
        region_text = self.view.substr(view_region)
        try:
            # Feed input into json.loads(), the use json.dumps() method
            # with indent option to send the formatted object to standard out.
            json_string = json.dumps(json.loads(region_text), sort_keys=False, indent=4)
            # Replace the view text with the output from json.dumps() method
            self.view.replace(edit, view_region, json_string)
        except:
            print("Not valid JSON.")